time=$(date +%M)
if [ $time -ge 55 ]
then
echo "Please Run Script after few minutes"
exit
fi
Device_List()
{
echo "Enter User Token"
read token
echo "Getting Device List"
result=$(curl -s -X GET -H "Authorization: Bearer $token" https://cyberv1p.io.xunison.com/api/v1/parental_control/devices)
if [[ "$result" = *200* ]]
then
echo "Device List: "
echo $result | jq -r ".devices"
else
echo "Cannot get device list"
echo "Error: "
echo $result
exit
fi
echo "Enter Device ID"
read device_id
}
Block_Internet()
{
echo "Blocking Internet..."
result=$(curl -s -X PUT -H "Content-Type: application/json" -H "Authorization: Bearer $token" -d "{\"internet_status\":-1}" https://cyberv1p.io.xunison.com/api/v1/parental_control/internetaccess?device_id=$device_id)
if [[ "$result" == *"FAILURE"* ]]
then
echo "Internet Blocking Failed"
echo "Error:"
echo $result
exit
else
echo "Internet Blocking Successful"
echo $result
fi
}
Unblock_Internet()
{
echo "Unblocking Internet..."
result=$(curl -s -X PUT -H "Content-Type: application/json" -H "Authorization: Bearer $token" -d "{\"internet_status\":1}" https://cyberv1p.io.xunison.com/api/v1/parental_control/internetaccess?device_id=$device_id)
if [[ "$result" == *"FAILURE"* ]]
then
echo "Internet UnBlocking Failed"
echo "Error:"
echo $result
exit
else
echo "Internet UnBlocking Successful"
echo $result
fi
}
start_youtube()
{
echo "Starting Youtube Streaming"
gnome-terminal -- "mpv https://www.youtube.com/watch?v=7bv_eqtkKqQ --fs -vo tct --network-timeout 1 --no-cache --quiet --cache-secs 2"
sleep 2
if !( ps -a | grep -q "mpv" )
then
echo "Youtube Streaming Failed"
exit
fi
echo "Waiting for 30 secs..."
sleep 30
}
start_facebook()
{
echo "Starting Facebook Streaming"
gnome-terminal -- "mpv https://www.facebook.com/TheInvincibleIndia/videos/varanasi-india-in-4k-ultra-hd/618275281844971/ --fs --vo tct --network-timeout 8 --no-cache --quiet --cache-secs 2"
sleep 2
if !( ps -a | grep -q "mpv" )
then
echo "Facebook Streaming Failed"
exit
fi
echo "Waiting for 30 secs..."
sleep 30
}
check_facebook()
{
if (ps -a | grep -q mpv)
then
echo "Testing Facebook streaming every 0.5 seconds for 1 min"
echo "**Facebook is still streaming**"
else
echo "**Facebook Streaming stopped**"
echo "facebook_test=Not Streaming" >> stream.report
killall watch
fi
}
check_youtube()
{
if (ps -a | grep -q mpv)
then
echo "Testing Youtube streaming every 0.5 seconds for 1 min"
echo "**Youtube is still streaming**"
else
echo "**Youtube Streaming stopped**"
echo "youtube_test=Not Streaming" >> stream.report
killall watch
fi
}
main()
{
Device_List
start_youtube
sleep 2
Block_Internet
sleep 2
export -f check_youtube
start=$(date +%M)
watch -n 0.5 -x bash -c check_youtube
end=$(date +%M)
time=$(echo $((end - start)))
echo "**Youtube Streaming Stopped in $time Minutes**"
Unblock_Internet
echo "Manually Unblock Internet for further testing in 30 secs..."
sleep 30
start_facebook
Block_Internet
export -f check_facebook
start=$(date +%M)
watch -n 0.5 -x bash -c check_facebook
end=$(date +%M)
time=$(echo $((end - start)))
echo $facebook
echo "**Facebook Streaming Stopped in $time Minutes**"
Unblock_Internet
echo "Testing Successfull, Manually Unblock internet from the panel for this device.."
sleep 2
}
main
